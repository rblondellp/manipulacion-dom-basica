const h1 = document.querySelector("h1");
const input1 = document.querySelector("#calculo1");
const input2 = document.querySelector("#calculo2");
const boton = document.querySelector("#btnCalcular");
const result = document.getElementById("result");
const form = document.getElementById("form");

form.addEventListener("submit", sumarInputValues);

function sumarInputValues(event) {
    event.preventDefault();
    const sumaInputs = parseInt(input1.value) + parseInt(input2.value);
    result.innerHTML = "Resultado: " + sumaInputs;
}
